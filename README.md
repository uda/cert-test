# Cert Test

Test certificate performance

## Usage

This test can currently be run in two ways:
* In a GitLab runner on the target environment
* On a machine that has `docker` and `apache2-utils` installed
