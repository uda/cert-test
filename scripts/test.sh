#!/usr/bin/env sh

mkdir -p results

source ./scripts/functions.sh

run_test 10 100
run_test 100 1000
run_test 100 5000
run_test 1000 10000
run_test 1000 50000
