function run_test() {
  C=$1
  N=$2
  ab -c $C -n $N https://127.0.0.1:8443/ > results/test_c${C}_n${N}_ec.txt
  ab -c $C -n $N https://127.0.0.2:8443/ > results/test_c${C}_n${N}_rsa.txt
}
