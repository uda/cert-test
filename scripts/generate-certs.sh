#!/usr/bin/env bash

apt-get update -qq && apt-get install -qq openssl

set -x

# CA

CA_NAME="${CA_NAME:-test}"
CA_PATH="${CA_PATH:-/ca}"
CA_DAYS=${CA_DAYS:-750}
CA_EC_SIZE=${CA_EC_SIZE:-prime256v1}
CA_RSA_SIZE=${CA_RSA_SIZE:-4096}

mkdir -p "${CA_PATH}"

openssl ecparam -name ${CA_EC_SIZE} -genkey -out "${CA_PATH}/${CA_NAME}-ec-ca.key"
openssl req -x509 -new -nodes -sha256 -days ${CA_DAYS} \
  -key "${CA_PATH}/${CA_NAME}-ec-ca.key" \
  -out "${CA_PATH}/${CA_NAME}-ec-ca.crt" \
  -subj "/C=US/ST=Florida/L=Saint Johns/O=x59 DevOps, LLC/OU=IT/CN=x59 DevOps EC-CA G1"

openssl genrsa -out "${CA_PATH}/${CA_NAME}-rsa-ca.key" ${CA_RSA_SIZE}
openssl req -x509 -new -nodes -sha256 -days ${CA_DAYS} \
  -key "${CA_PATH}/${CA_NAME}-rsa-ca.key" \
  -out "${CA_PATH}/${CA_NAME}-rsa-ca.crt" \
  -subj "/C=US/ST=Florida/L=Saint Johns/O=x59 DevOps, LLC/OU=IT/CN=x59 DevOps RSA-CA G1"

# Key + CSR

CRT_PATH="${CRT_PATH:-/crt}"
SERVER_NAME="${SERVER_NAME:-test}"
CRT_CA_SIZE=${CRT_CA_SIZE:-prime256v1}
CRT_RSA_SIZE=${CRT_RSA_SIZE:-4096}
CRT_HOSTNAME_TLD=${CRT_HOSTNAME:-local}

mkdir -p "${CRT_PATH}"

cat > "${CRT_PATH}/${SERVER_NAME}-ec-server.conf" << EOF
[ req_distinguished_name ]
[ req ]
distinguished_name = req_distinguished_name
req_extensions = v3_req
[ v3_req ]
extendedKeyUsage=serverAuth,clientAuth
subjectAltName=DNS:ec.${CRT_HOSTNAME_BASE},IP:127.0.0.1
EOF

cat > "${CRT_PATH}/${SERVER_NAME}-rsa-server.conf" << EOF
[ req_distinguished_name ]
[ req ]
distinguished_name = req_distinguished_name
req_extensions = v3_req
[ v3_req ]
extendedKeyUsage=serverAuth,clientAuth
subjectAltName=DNS:rsa.${CRT_HOSTNAME_BASE},IP:127.0.0.2
EOF

openssl ecparam -name ${CRT_CA_SIZE} -genkey -out "${CRT_PATH}/${SERVER_NAME}-ec-server.key"
openssl req -new -nodes \
  -config "${CRT_PATH}/${SERVER_NAME}-ec-server.conf" \
  -key "${CRT_PATH}/${SERVER_NAME}-ec-server.key" \
  -out "${CRT_PATH}/${SERVER_NAME}-ec-server.csr" \
  -subj "/CN=ec.${CRT_HOSTNAME_TLD}/C=US/ST=Florida/L=Saint Johns/O=x59 DevOps, LLC"

openssl genrsa -out "${CRT_PATH}/${SERVER_NAME}-rsa-server.key" ${CRT_RSA_SIZE}
openssl req -new -nodes \
  -config "${CRT_PATH}/${SERVER_NAME}-rsa-server.conf" \
  -key "${CRT_PATH}/${SERVER_NAME}-rsa-server.key" \
  -out "${CRT_PATH}/${SERVER_NAME}-rsa-server.csr" \
  -subj "/CN=rsa.${CRT_HOSTNAME_TLD}/C=US/ST=Florida/L=Saint Johns/O=x59 DevOps, LLC"

# Sign

cat > "${CRT_PATH}/${SERVER_NAME}-ec-crt.conf" << EOF
  authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  subjectAltName = @alt_names
  [alt_names]
  DNS.1 = ec.${CRT_HOSTNAME_BASE}
  IP.1 = 127.0.0.1
EOF

cat > "${CRT_PATH}/${SERVER_NAME}-rsa-crt.conf" << EOF
  authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  subjectAltName = @alt_names
  [alt_names]
  DNS.1 = rsa.${CRT_HOSTNAME_BASE}
  IP.1 = 127.0.0.2
EOF

CRT_DAYS=${CRT_DAYS:-365}

openssl x509 -req -sha256 -days ${CRT_DAYS} -CAcreateserial \
  -CA "${CA_PATH}/${CA_NAME}-ec-ca.crt" \
  -CAkey "${CA_PATH}/${CA_NAME}-ec-ca.key" \
  -extfile "${CRT_PATH}/${SERVER_NAME}-ec-crt.conf" \
  -in "${CRT_PATH}/${SERVER_NAME}-ec-server.csr" \
  -out "${CRT_PATH}/${SERVER_NAME}-ec-server.crt"

openssl x509 -req -sha256 -days ${CRT_DAYS} -CAcreateserial \
  -CA "${CA_PATH}/${CA_NAME}-rsa-ca.crt" \
  -CAkey "${CA_PATH}/${CA_NAME}-rsa-ca.key" \
  -extfile "${CRT_PATH}/${SERVER_NAME}-rsa-crt.conf" \
  -in "${CRT_PATH}/${SERVER_NAME}-rsa-server.csr" \
  -out "${CRT_PATH}/${SERVER_NAME}-rsa-server.crt"
