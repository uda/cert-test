#!/usr/bin/env bash

docker run --rm -ti -v `pwd`:/p --env CA_PATH=/p/certs --env CRT_PATH=/p/certs debian:bookworm-slim /p/scripts/generate-certs.sh
docker run -d --name nginx-test -v `pwd`/certs:/certs -v `pwd`/default.conf:/etc/nginx/conf.d/default.conf -p 8443:8443 nginx:alpine

./scripts/test.sh

docker stop nginx-test
docker rm nginx-test
